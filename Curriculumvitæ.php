<?php require 'Navigation.php' ?>

<head>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous"></head>
    <link rel="stylesheet" href="css/Home.css">
    <script src="js/Data.js"></script>
</head>
<body>
<div class="jumbotron card card-image" style="background-image: url(https://mdbootstrap.com/img/Photos/Others/gradient1.jpg);">
  <div class="text-white text-center py-5 px-4">
    <div>
      <h2 class="card-title h1-responsive pt-3 mb-5 font-bold"><strong>Create your beautiful website with MDBootstrap</strong></h2>
      <p class="mx-5 mb-5">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Repellat fugiat, laboriosam, voluptatem,
        optio vero odio nam sit officia accusamus minus error nisi architecto nulla ipsum dignissimos. Odit sed qui, dolorum!
      </p>
      <a class="btn btn-outline-white btn-md"><i class="fas fa-clone left"></i> View project</a>
    </div>
  </div>
</div>
<h4 class="text-center py-4">Two equal columns layout</h4>

<!-- Grid row -->
<div class="row">

  <!-- Grid column -->
  <div class="col-md-6 mb-4">

    <!--Card-->
    <div class="card default-color-dark">

      <!--Card image-->
      <div class="view">
        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(125).jpg" class="card-img-top" alt="photo">
        <a href="#">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body text-center">
        <!--Title-->
        <h4 class="card-title white-text">Title of the news</h4>
        <!--Text-->
        <p class="card-text white-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et
          quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
        <a href="#" class="btn btn-outline-white btn-md waves-effect">Button</a>
      </div>

    </div>
    <!--/.Card-->

  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-md-6 mb-4">

    <!--Card-->
    <div class="card primary-color-dark">

      <!--Card image-->
      <div class="view">
        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(122).jpg" class="card-img-top" alt="photo">
        <a href="#">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body text-center">
        <!--Title-->
        <h4 class="card-title white-text">Title of the news</h4>
        <!--Text-->
        <p class="card-text white-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et
          quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
        <a href="#" class="btn btn-outline-white btn-md waves-effect">Button</a>
      </div>

    </div>
    <!--/.Card-->

  </div>
  <!-- Grid column -->

</div>
<!-- Grid row -->

<h4 class="text-center py-4">Three equal columns layout</h4>

<!-- Grid row -->
<div class="row">

  <!-- Grid column -->
  <div class="col-lg-4 col-md-12 mb-4">

    <!--Card Primary-->
    <div class="card mdb-color text-center z-depth-2">
      <div class="card-body">
        <h3 class="text-uppercase font-weight-bold cyan-text mt-2 mb-3"><strong>First column</strong></h3>
        <p class="white-text mb-0">Ut enim ad minima veniam, quis nostrum exercita tionem ullam corporis
          suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis autem vel eum iure reprehenderit
          qui in ea voluptate non proident velit esse quam. </p>
      </div>
    </div>
    <!--/.Card Primary-->

  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-lg-4 col-md-6 mb-4">

    <!--Card Primary-->
    <div class="card red darken-4 text-center z-depth-2">
      <div class="card-body">
        <h3 class="text-uppercase font-weight-bold light-green-text mt-2 mb-3"><strong>Second column</strong></h3>
        <p class="white-text mb-0">Ut enim ad minima veniam, quis nostrum exercita tionem ullam corporis
          suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis autem vel eum iure reprehenderit
          qui in ea voluptate non proident velit esse quam. </p>
      </div>
    </div>
    <!--/.Card Primary-->

  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-lg-4 col-md-6 mb-4">

    <!--Card Primary-->
    <div class="card light-blue darken-1 text-center z-depth-2">
      <div class="card-body">
        <h3 class="text-uppercase font-weight-bold purple-text mt-2 mb-3"><strong>Third column</strong></h3>
        <p class="white-text mb-0">Ut enim ad minima veniam, quis nostrum exercita tionem ullam corporis
          suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur. Quis autem vel eum iure reprehenderit
          qui in ea voluptate non proident velit esse quam. </p>
      </div>
    </div>
    <!--/.Card Primary-->

  </div>
  <!-- Grid column -->

</div>
<!-- Grid row -->

<h4 class="text-center py-4">Two different columns layout</h4>

<!-- Grid row -->
<div class="row">

  <!-- Grid column -->
  <div class="col-md-6 col-lg-7 mb-4">

    <!--Card-->
    <div class="card">

      <!--Card image-->
      <div class="view">
        <img src="https://mdbootstrap.com/img/Photos/Slides/img%20(112).jpg" class="card-img-top" alt="photo">
        <a href="#">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body text-center">
        <!--Title-->
        <h4 class="card-title">Nature is beautiful!</h4>
        <!--Text-->
        <p class="card-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium
          doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi
          architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas.</p>
        <a href="#" class="btn btn-dark-green">Read more</a>
      </div>

    </div>
    <!--/.Card-->

  </div>
  <!-- Grid column -->

  <!-- Grid column -->
  <div class="col-md-6 col-lg-5">

    <!--Card-->
    <div class="card green darken-3">

      <!--Card image-->
      <div class="view">
        <img src="https://mdbootstrap.com/img/Photos/Horizontal/Nature/4-col/img%20(131).jpg" class="card-img-top"
          alt="photo">
        <a href="#">
          <div class="mask rgba-white-slight"></div>
        </a>
      </div>

      <!--Card content-->
      <div class="card-body text-center">
        <!--Title-->
        <h4 class="card-title white-text">Read about polish plants</h4>
        <!--Text-->
        <p class="card-text white-text">Sed ut perspiciatis unde omnis iste natus error sit voluptatem
          accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et
          quasi architecto vitae.</p>
        <a href="#" class="btn btn-outline-white btn-md waves-effect">Read more</a>
      </div>

    </div>
    <!--/.Card-->

  </div>
  <!-- Grid column -->

</div>
<!-- Grid row -->
<canvas id="labelChart"></canvas>

</body>
<?php require 'Footer.php' ?>
